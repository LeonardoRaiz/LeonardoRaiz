<div id="header" align="center">
  <img src="https://media.tenor.com/NlnpmndaKagAAAAi/mixflavor-coko.gif" width="100"/>
  <div id="badges">
    <a href="https://www.linkedin.com/in/leonardoraiz">
      <img src="https://img.shields.io/badge/LinkedIn-blue?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/>
    </a>
    <a href="https://www.instagram.com/leonraiz">
      <img src="https://img.shields.io/badge/Instagram-blueviolet?style=for-the-badge&logo=instagram&logoColor=white" alt="Youtube Badge"/>
    </a>
    <a href="https://leonardoraiz.itch.io/">
      <img src="https://img.shields.io/badge/itch.io-1AC3ED?style=for-the-badge&logo=itch.io&logoColor=white" alt="Twitter Badge"/>
    </a>
  </div>
  <img src="https://komarev.com/ghpvc/?username=leonardoraiz&style=flat-square&color=AFED4C" alt=""/>
  <br/>
  <h3> Hello There! <img src="https://www.shareicon.net/data/128x128/2016/11/21/854790_kenobi_512x512.png" width="50px"/></h3>
  
  
</div>


### 👨‍💻: About Me :

- 👋 Hi, I’m @LeonardoRaiz
- 👀 I’m interested in create video games
- 🌱 I’m currently learning Angular
- 💞️ I’m looking to collaborate on creation of games, both artistically and programming
- 📫 How to reach me leonardoraiz@icloud.com or https://www.instagram.com/leonraiz/

<!---
LeonardoRaiz/LeonardoRaiz is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.

-->
***

 <div align="center">
  <a href="https://github.com/leonardoraiz">
  <img height="150em" src="https://github-readme-stats.vercel.app/api?username=leonardoraiz&show_icons=true&theme=dracula&include_all_commits=true&count_private=true"/>
  <img height="150em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=leonardoraiz&layout=compact&langs_count=7&theme=dracula"/>

  
  [![GitHub Streak](http://github-readme-streak-stats.herokuapp.com?user=leonardoraiz&theme=dark&background=000000)](https://git.io/streak-stats)
</div>

***
  
### 🖱️: Main Technologies : 
<div style="display: inline_block"><br>
  <img align="center" alt="Unity" height="30" width="40" src="https://uixlibrary.com/uploads/icons/62ee2d16cb458f0e8150cfbe-42878531-unity.svg" />
  <img align="center" alt="SQL-server" height="30" width="40" src="https://www.svgrepo.com/show/303229/microsoft-sql-server-logo.svg" />
  <img align="center" alt="Csharp" height="30" width="40" src="https://cdn.worldvectorlogo.com/logos/c--4.svg" />
  <img align="center" alt="git" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/git/git-original.svg">
  <img align="center" alt="Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img align="center" alt="HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="CSS" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
  <img align="center" alt="Blender" height="30" width="40" src="https://cdn.worldvectorlogo.com/logos/blender-2.svg">
  <img align="center" alt="Substance" height="30" width="40" src="https://cdn.worldvectorlogo.com/logos/substance-painter.svg">
</div>
  
